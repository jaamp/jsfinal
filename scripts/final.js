var playButton = document.getElementById('play-button');
var resetButton = document.getElementById('reset-button');
var toggleCards = document.getElementsByClassName('iCards');
var toggleScenes = document.getElementsByClassName('scenes');
var playInstruct = document.getElementById('instruct');
var winInstruct = document.getElementById('winner');
var loseInstruct = document.getElementById('loser');
var messName = document.getElementById('mess-name');
var maEl = document.getElementById('ma-title');
var messageEl = document.getElementById('ma-message');
var foreEl = document.getElementById('foreCast');
var highEl = document.getElementById('highTemp');
var lowEl = document.getElementById('lowTemp');
var submitter = document.getElementById('submits');

var ak = '42190_PC';
var ha = '40717_PC';
var dv = '38639_PC';
var br = '12198_PC';
var uw = '41393_PC';
var API_KEY = '';
var URL1 = 'http://dataservice.accuweather.com/forecasts/v1/daily/1day/';
var URL2 = '?apikey=';
var firstFlip;
var secondFlip;
var url;
loseInstruct.style.display = 'none';
winInstruct.style.display = 'none';
playInstruct.style.display = 'none';
resetButton.style.display = 'none';
playButton.style.display = 'inline-block';
localStorage.clear();

var errorMessage = function (inputEl, message) {
    let errorEl = inputEl.parentNode.querySelector('.error');
    errorEl.innerText = message;
}

submitter.addEventListener('click', function (e) {
    e.stopPropagation(); e.preventDefault();

    var fNameInput = document.getElementById('fName');
    var lNameInput = document.getElementById('lName');
    var emailInput = document.getElementById('email');

    localStorage.fNameInput2 = document.getElementById('fName').value;

    if (fNameInput.value.length < 2) {
        e.preventDefault();
        errorMessage(fNameInput, 'Name must be 2 or more characters');
    }

    if (lNameInput.value.length < 3) {
        e.preventDefault();
        errorMessage(lNameInput, 'Name must be 3 or more characters');
    }

    if (!/\w+@\w+\.\w+/.test(emailInput.value)) {
        e.preventDefault();
        errorMessage(emailInput, 'Email required, must be in valid format');
    }

});


for (let i = 0; i < toggleCards.length; i++) {
    toggleCards[i].style.display = 'inline-block';
}
for (let i = 0; i < toggleScenes.length; i++) {
    toggleScenes[i].style.display = 'none';
}

playButton.addEventListener('click', function (e) {
    
    e.stopPropagation();

    messName.innerText = "Hello " +localStorage.fNameInput2+ ", I got your name from your browser's local storage"; 

    for (let i = 0; i < toggleCards.length; i++) {
        if (toggleCards[i].style.display == 'inline-block') {
            toggleCards[i].style.display = 'none';
        }
        else {
            toggleCards[i].style.display = 'inline-block';
        }
    }
    for (let i = 0; i < toggleScenes.length; i++) {
        if (toggleScenes[i].style.display == 'inline-block') {
            toggleScenes[i].style.display = 'none';
        }
        else {
            toggleScenes[i].style.display = 'inline-block';
        }
        playButton.style.display = 'none';
    }
    setTimeout(function () {
        for (let i = 0; i < toggleCards.length; i++) {
            if (toggleCards[i].style.display == 'inline-block') {
                toggleCards[i].style.display = 'none';
            }
            else {
                toggleCards[i].style.display = 'inline-block';
            }
        }
        for (let i = 0; i < toggleScenes.length; i++) {
            if (toggleScenes[i].style.display == 'inline-block') {
                toggleScenes[i].style.display = 'none';
            }
            else {
                toggleScenes[i].style.display = 'inline-block';
            }
            playInstruct.style.display = 'inline-block';
        }
    }, 1000);
    cardFlipper();
});

function cardFlipper() {
    const flipCard = document.getElementsByClassName('cards');
    var j = 0;
    for (let i = 0; i < flipCard.length; i++) {
        flipCard[i].addEventListener('click', function () {
            if (j == 0) {
                flipCard[i].firstElementChild.style.display = 'none';
                flipCard[i].lastElementChild.style.display = 'inline-block';
                firstFlip = flipCard[i].lastElementChild.attributes.alt.value;
                j++;
            }
            else {
                flipCard[i].firstElementChild.style.display = 'none';
                flipCard[i].lastElementChild.style.display = 'inline-block';
                secondFlip = flipCard[i].lastElementChild.attributes.alt.value;
                j++;
            }
            if (j == 2) {
                if (firstFlip == secondFlip) {
                    playInstruct.style.display = 'none';
                    resetButton.style.display = 'inline-block';
                    winInstruct.style.display = "inline-block";
                    switch (firstFlip) {
                        case 'ak':
                            url = URL1 + ak + URL2 + API_KEY;
                            var akWinner = 'Congratulations You Made it to Alaska';
                            var akMessage = 'You have matched Alaska, and now the Denali Weather';
                            maEl.innerText = akWinner;
                            messageEl.innerText = akMessage;
                            break;
                        case 'ha':
                            url = URL1 + ha + URL2 + API_KEY;
                            var haWinner = 'Congratulations You Made it to Hawaii';
                            var haMessage = 'You have matched Hawaii, and now the Wikki Beach Weather';
                            maEl.innerText = haWinner;
                            messageEl.innerText = haMessage;
                            break;
                        case 'br':
                            url = URL1 + br + URL2 + API_KEY;
                            var brWinner = 'Congratulations You Made it to North Carolina';
                            var brMessage = 'You have matched NC, and now the Blue Ridge Weather';
                            maEl.innerText = brWinner;
                            messageEl.innerText = brMessage;
                            break;
                        case 'dv':
                            url = URL1 + dv + URL2 + API_KEY;
                            var dvWinner = 'Congratulations You Made it to Death Valley';
                            var dvMessage = 'You have matched Death Valley, and now the Desert Weather';
                            maEl.innerText = dvWinner;
                            messageEl.innerText = dvMessage;
                            break;
                        default:
                            console.log('ugh');
                            break;
                    }
                    setMessage();
                }
                else {
                    playInstruct.style.display = 'none';
                    resetButton.style.display = 'inline-block';
                    loseInstruct.style.display = "inline-block"
                    url = URL1 + uw + URL2 + API_KEY;
                    var uwWinner = 'Sorry No Match';
                    var uwMessage = 'As a consolation here is the UW Weather';
                    maEl.innerText = uwWinner;
                    messageEl.innerText = uwMessage;
                    setMessage();
                }
            }
        });
    };
}

resetButton.addEventListener('click', function () {
    location.reload();
});

function setMessage() {
    fetch(url).then((response) => response.json())
        .then(function (response) {
            var highTemp = response.DailyForecasts[0].Temperature.Maximum.Value;
            var lowTemp = response.DailyForecasts[0].Temperature.Minimum.Value;
            var weather = response.Headline.Text;
            var highMessage = "Today's High Temperature: " + highTemp + "F";
            var lowMessage = "Today's Low Temperature: " + lowTemp + "F";
            foreEl.innerText = weather;
            highEl.innerText = highMessage;
            lowEl.innerText = lowMessage;
        });
}

